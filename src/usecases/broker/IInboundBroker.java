package usecases.broker;

import java.util.List;
import models.Transaction;

public interface IInboundBroker {
    void addSubscriber(ISubscriber subscriber);

    void broadcast(List<Transaction> transactions);

    void run() throws Exception;
}
