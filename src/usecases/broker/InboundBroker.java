package usecases.broker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import com.google.gson.*;
import models.Transaction;

public class InboundBroker implements IInboundBroker {
    Connection amqpConnection;
    String queueName;

    List<ISubscriber> subscriberList;

    public InboundBroker(Connection amqpConnection, String queueName) {
        this.amqpConnection = amqpConnection;
        this.queueName = queueName;
        this.subscriberList = new ArrayList<ISubscriber>();
    }

    @Override
    public void addSubscriber(ISubscriber subscriber) {
        subscriberList.add(subscriber);
    }

    @Override
    public void broadcast(List<Transaction> transactions) {
        for (ISubscriber subscriber : subscriberList) {
            try {
                subscriber.process(transactions);
            } catch (Exception error) {
                System.out.println(error.getMessage());
            }
        }
    }

    @Override
    public void run() throws Exception {
        Channel channel = amqpConnection.createChannel();

        channel.queueDeclare(queueName, true, false, false, null);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String payload = new String(delivery.getBody());
            Gson gson = new Gson();
            List<Transaction> transactions = Arrays.asList(gson.fromJson(payload, Transaction[].class));

            broadcast(transactions);
        };

        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {});
    }
}
