package usecases.broker;

import java.util.List;
import models.Transaction;

public interface ISubscriber {
    public void process(List<Transaction> transactions);
}
