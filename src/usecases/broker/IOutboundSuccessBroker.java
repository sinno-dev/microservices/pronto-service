package usecases.broker;

import java.util.List;
import models.Transaction;

public interface IOutboundSuccessBroker {
    void publish(List<Transaction> transactions) throws Exception;
}
