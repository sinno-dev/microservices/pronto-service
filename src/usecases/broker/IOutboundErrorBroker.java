package usecases.broker;

import java.util.List;
import models.Transaction;

public interface IOutboundErrorBroker {
    void publish(List<Transaction> transactions, Exception error) throws Exception;
}
