package usecases.broker;

import java.util.List;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import models.Transaction;

public class OutboundErrorBroker implements IOutboundErrorBroker {
    Connection amqpConnection;
    String queueName;

    public OutboundErrorBroker(Connection amqpConnection, String queueName) {
        this.amqpConnection = amqpConnection;
        this.queueName = queueName;
    }

    @Override
    public void publish(List<Transaction> transactions, Exception error) throws Exception {
        Channel channel = amqpConnection.createChannel();
        Gson gson = new Gson();

        channel.queueDeclare(queueName, true, false, false, null);

        String payload = gson.toJson(transactions);

        channel.basicPublish("", queueName, null, payload.getBytes());
        channel.close();
    }
}
