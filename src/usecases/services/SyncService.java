package usecases.services;

import java.util.List;
import models.Transaction;
import repositories.ITransactionRepository;
import usecases.broker.IOutboundErrorBroker;
import usecases.broker.IOutboundSuccessBroker;

public class SyncService implements ISyncService {
    ITransactionRepository transactionRepository;

    IOutboundSuccessBroker outboundSuccessBroker;
    IOutboundErrorBroker outboundErrorBroker;

    public SyncService(ITransactionRepository transactionRepository, IOutboundSuccessBroker outboundSuccessBroker, IOutboundErrorBroker outboundErrorBroker) {
        this.transactionRepository = transactionRepository;
        this.outboundSuccessBroker = outboundSuccessBroker;
        this.outboundErrorBroker = outboundErrorBroker;
    }

    @Override
    public void sync(List<Transaction> transactions) {
        try {
            transactionRepository.batchCreate(transactions);

            outboundSuccessBroker.publish(transactions);
        } catch (Exception error) {
            System.out.println("[error] " + error.getMessage());

            try {
                outboundErrorBroker.publish(transactions, error);
            } catch (Exception brokerError) {
                System.out.println("[error] " + brokerError.getMessage());
            }
        }
    }
}
