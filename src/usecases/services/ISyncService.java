package usecases.services;

import models.Transaction;

import java.util.List;

public interface ISyncService {
    void sync(List<Transaction> transactions);
}
