package models;

import com.google.gson.annotations.SerializedName;

public class Transaction {
    @SerializedName("sinnoDisbursementId")
    public int sinnoDisbursementId;

    @SerializedName("tmp_gl_accountcode")
    public String accountCode;

    @SerializedName("gl_subledger_code")
    public String subledgerCode;

    @SerializedName("tmp_gl_other_side")
    public String otherSide;

    @SerializedName("tmp_gl_trans_type")
    public String transactionType;

    @SerializedName("tmp_gl_trans_date")
    public String transactionDate;

    @SerializedName("dcl_tmp_gl_reference")
    public String reference;

    @SerializedName("dcl_tmp_gl_details")
    public String details;

    @SerializedName("dcl_tmp_gl_amount")
    public float amount;

    @SerializedName("tmp_gl_tr_job_code")
    public String jobCode;

    @SerializedName("special_account_n")
    public String specialAccountN;

    @SerializedName("dcl_tmp_gl_trans_no")
    public int transactionNumber;

    @SerializedName("master_updated_flag")
    public String masterUpdatedFlag;

    @SerializedName("tr_user_only_date1")
    public String userOnlyDate1;

    @SerializedName("tr_user_only_alpha4")
    public String userOnlyAlpha4;

    @SerializedName("gl_tr_user_only_num1")
    public int userOnlyNum1;

    @SerializedName("gl_tr_user_only_num2")
    public int userOnlyNum2;

    @SerializedName("gl_tr_analysis_code1")
    public String analysisCode1;

    @SerializedName("tmp_gl_tr_asset_no")
    public String assetNumber;

    @SerializedName("gl_tr_document_type")
    public String documentType;

    @SerializedName("gl_tr_document_no")
    public String documentNumber;

    @SerializedName("tr_document_suffix")
    public String documentSuffix;

    @SerializedName("gl_tr_document_seq")
    public int documentSequence;

    @SerializedName("gl_tr_entity_code")
    public String entityCode;

    @SerializedName("tr_transacted_curr_code")
    public String currencyCode;

    @SerializedName("gl_tr_trans_curr_amt")
    public float currencyAmount;

    @SerializedName("gl_tr_analysis_code2")
    public String analysisCode2;

    @SerializedName("gl_tr_financial_year")
    public int financialYear;

    @SerializedName("tr_financial_period")
    public int financialPeriod;

    @SerializedName("dcl_tmp_gl_tr_record_status")
    public String recordStatus;

    @SerializedName("dcl_tmp_currency_rate")
    public float currencyRate;

    @SerializedName("dcl_tmp_tr_pa_posting_date")
    public String paPostingDate;

    @SerializedName("tr_user_only_alpha2")
    public String userOnlyAlpha2;
}
