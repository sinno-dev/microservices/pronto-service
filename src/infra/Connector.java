package infra;

import com.informix.jdbc.IfxDriver;
import com.rabbitmq.client.ConnectionFactory;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Properties;

public class Connector {
    public static Connection connectDb(String host, String port, String server, String database, String user, String password) throws Exception {
        Driver ifxDriver = new IfxDriver();
        DriverManager.registerDriver(ifxDriver);

        String connectionString = "jdbc:informix-sqli://" + host + ":" + port + "/" + database + ":INFORMIXSERVER=" + server;
        Properties connectionInfo = new Properties();
        connectionInfo.put("user", user);
        connectionInfo.put("password", password);

        return DriverManager.getConnection(connectionString, connectionInfo);
    }

    public static com.rabbitmq.client.Connection connectAmqp(String host, String port, String user, String password) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost(host);
        factory.setPort(Integer.parseInt(port));
        factory.setUsername(user);
        factory.setPassword(password);

        return factory.newConnection();
    }
}
