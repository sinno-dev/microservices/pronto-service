import java.sql.Connection;
import infra.Connector;
import repositories.ITransactionRepository;
import repositories.TransactionRepository;
import usecases.broker.*;
import usecases.services.SyncService;
import usecases.services.ISyncService;

public class Main {
    public static void main(String[] args) {
        try {
            Connection dbConn = Connector.connectDb(
                    System.getenv("IFX_HOST"),
                    System.getenv("IFX_PORT"),
                    System.getenv("IFX_SERVER"),
                    System.getenv("IFX_DATABASE"),
                    System.getenv("IFX_USER"),
                    System.getenv("IFX_PASSWORD")
            );
            com.rabbitmq.client.Connection amqpConn = Connector.connectAmqp(
                    System.getenv("RABBITMQ_HOST"),
                    System.getenv("RABBITMQ_PORT"),
                    System.getenv("RABBITMQ_USER"),
                    System.getenv("RABBITMQ_PASSWORD")
            );

            ITransactionRepository transactionRepository = new TransactionRepository(dbConn);
            IInboundBroker inboundBroker = new InboundBroker(amqpConn, System.getenv("RABBITMQ_INBOUND_QUEUE"));
            IOutboundSuccessBroker outboundSuccessBroker = new OutboundSuccessBroker(amqpConn, System.getenv("RABBITMQ_OUTBOUND_SUCCESS_QUEUE"));
            IOutboundErrorBroker outboundErrorBroker = new OutboundErrorBroker(amqpConn, System.getenv("RABBITMQ_OUTBOUND_ERROR_QUEUE"));
            ISyncService syncService = new SyncService(transactionRepository, outboundSuccessBroker, outboundErrorBroker);

            inboundBroker.addSubscriber(transactions -> syncService.sync(transactions));

            System.out.println("[*] Listening for transactions");
            inboundBroker.run();
        } catch (Exception error) {
            System.out.println(error.getMessage());
        }
    }
}
