package repositories;

import java.util.List;
import models.Transaction;

public interface ITransactionRepository {
    void batchCreate(List<Transaction> transactions) throws Exception;
}
