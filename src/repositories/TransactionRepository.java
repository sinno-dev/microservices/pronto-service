package repositories;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import models.Transaction;

public class TransactionRepository implements ITransactionRepository {
    Connection connection;

    String[] columns = {
            "tmp_gl_accountcode",
            "gl_subledger_code",
            "tmp_gl_other_side",
            "tmp_gl_trans_type",
            "tmp_gl_trans_date",
            "dcl_tmp_gl_reference",
            "dcl_tmp_gl_details",
            "dcl_tmp_gl_amount",
            "tmp_gl_tr_job_code",
            "special_account_n",
            "dcl_tmp_gl_trans_no",
            "master_updated_flag",
            "tr_user_only_date1",
            "tr_user_only_alpha4",
            "gl_tr_user_only_num1",
            "gl_tr_user_only_num2",
            "gl_tr_analysis_code1",
            "tmp_gl_tr_asset_no",
            "gl_tr_document_type",
            "gl_tr_document_no",
            "tr_document_suffix",
            "gl_tr_document_seq",
            "gl_tr_entity_code",
            "tr_transacted_curr_code",
            "gl_tr_trans_curr_amt",
            "gl_tr_analysis_code2",
            "gl_tr_financial_year",
            "tr_financial_period",
            "dcl_tmp_gl_tr_record_status",
            "dcl_tmp_currency_rate",
            "dcl_tmp_tr_pa_posting_date",
            "tr_user_only_alpha2"
    };

    public TransactionRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void batchCreate(List<Transaction> transactions) throws Exception {
        Statement statement = null;

        try {
            String sql = generateSql(transactions);

            statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (Exception error) {
            throw error;
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    protected String generateSql(List<Transaction> transactions) {
        String sql = "INSERT INTO dcltmpgltrans1 (" + String.join(", ", columns) + ")";
        List<String> values = new ArrayList();

        for (Transaction transaction : transactions) {
            List<String> value = new ArrayList();

            value.add("'" + transaction.accountCode + "'");
            value.add("'" + transaction.subledgerCode + "'");
            value.add("'" + transaction.otherSide + "'");
            value.add("'" + transaction.transactionType + "'");
            value.add("'" + transaction.transactionDate + "'");
            value.add("'" + transaction.reference + "'");
            value.add("'" + transaction.details + "'");
            value.add(String.valueOf(transaction.amount));
            value.add("'" + transaction.jobCode + "'");
            value.add("'" + transaction.specialAccountN + "'");
            value.add(String.valueOf(transaction.transactionNumber));
            value.add("'" + transaction.masterUpdatedFlag + "'");
            value.add("'" + transaction.userOnlyDate1 + "'");
            value.add("'" + transaction.userOnlyAlpha4 + "'");
            value.add(String.valueOf(transaction.userOnlyNum1));
            value.add(String.valueOf(transaction.userOnlyNum2));
            value.add("'" + transaction.analysisCode1 + "'");
            value.add("'" + transaction.assetNumber + "'");
            value.add("'" + transaction.documentType + "'");
            value.add("'" + transaction.documentNumber + "'");
            value.add("'" + transaction.documentSuffix + "'");
            value.add(String.valueOf(transaction.documentSequence));
            value.add("'" + transaction.entityCode + "'");
            value.add("'" + transaction.currencyCode + "'");
            value.add(String.valueOf(transaction.currencyAmount));
            value.add("'" + transaction.analysisCode2 + "'");
            value.add(String.valueOf(transaction.financialYear));
            value.add(String.valueOf(transaction.financialPeriod));
            value.add("'" + transaction.recordStatus + "'");
            value.add(String.valueOf(transaction.currencyRate));
            value.add("'" + transaction.paPostingDate + "'");
            value.add("'" + transaction.userOnlyAlpha2 + "'");

            values.add("(" + String.join(", ", value) + ")");
        }

        sql += " VALUES " + String.join(", ", values);

        return sql;
    }
}
