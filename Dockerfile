FROM openjdk:8-alpine AS build

WORKDIR /build

ADD . .
RUN mkdir out

WORKDIR /build/src

RUN javac \
    -d ../out \
    -Xlint:unchecked \
    -cp .:../resources/external/amqp-client-5.9.0.jar:../resources/external/gson-2.8.6.jar:../resources/external/ifxjdbc.jar:../resources/external/slf4j-api-1.7.9.jar:../resources/external/slf4j-simple-1.7.9.jar \
    Main.java

FROM openjdk:8-alpine

WORKDIR /app

COPY --from=build /build/out .
ADD ./resources/external ./resources/external

CMD ["java", "-cp", ".:./resources/external/amqp-client-5.9.0.jar:./resources/external/gson-2.8.6.jar:./resources/external/ifxjdbc.jar:./resources/external/slf4j-api-1.7.9.jar:./resources/external/slf4j-simple-1.7.9.jar", "Main"]
